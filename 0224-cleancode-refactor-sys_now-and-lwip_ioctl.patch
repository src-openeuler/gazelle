From 107e0eda4e255dc93aa94a9ffa31427f912341f5 Mon Sep 17 00:00:00 2001
From: Lemmy Huang <huangliming5@huawei.com>
Date: Fri, 12 Jul 2024 11:02:59 +0800
Subject: [PATCH] cleancode: refactor sys_now and lwip_ioctl

Signed-off-by: Lemmy Huang <huangliming5@huawei.com>
---
 src/common/gazelle_dfx_msg.h            |  8 ++++-
 src/lstack/api/lstack_epoll.c           |  4 ++-
 src/lstack/api/lstack_wrap.c            | 47 +++++++++++--------------
 src/lstack/core/lstack_cfg.c            |  1 -
 src/lstack/core/lstack_control_plane.c  | 21 ++++++-----
 src/lstack/core/lstack_dpdk.c           | 10 +++---
 src/lstack/core/lstack_init.c           |  2 --
 src/lstack/core/lstack_lwip.c           | 19 +++++-----
 src/lstack/core/lstack_protocol_stack.c | 12 +++----
 src/lstack/core/lstack_stack_stat.c     | 29 +++------------
 src/lstack/include/lstack_stack_stat.h  |  1 -
 src/lstack/include/lstack_thread_rpc.h  |  1 -
 src/lstack/netif/lstack_ethdev.c        |  3 +-
 src/ltran/ltran_forward.c               | 10 +++---
 src/ltran/ltran_stat.c                  |  2 +-
 src/ltran/ltran_stat.h                  |  6 ----
 src/ltran/ltran_timer.c                 | 14 +++-----
 src/ltran/ltran_timer.h                 |  4 +--
 test/unitest/stub.c                     |  6 ----
 19 files changed, 79 insertions(+), 121 deletions(-)

diff --git a/src/common/gazelle_dfx_msg.h b/src/common/gazelle_dfx_msg.h
index a2ec07b..8eb54ff 100644
--- a/src/common/gazelle_dfx_msg.h
+++ b/src/common/gazelle_dfx_msg.h
@@ -87,6 +87,12 @@ enum GAZELLE_LATENCY_TYPE {
     GAZELLE_LATENCY_MAX,
 };
 
+enum GAZELLE_TCP_LIST_STATE {
+    GAZELLE_ACTIVE_LIST,
+    GAZELLE_LISTEN_LIST,
+    GAZELLE_TIME_WAIT_LIST,
+};
+
 struct gazelle_stack_stat {
     uint64_t wakeup_events;
     uint64_t write_lwip_cnt;
@@ -240,7 +246,6 @@ struct gazelle_stat_lstack_proto {
 };
 
 
-/* same as define in lwip/tcp.h - struct tcp_pcb_dp */
 struct gazelle_stat_lstack_conn_info {
     uint32_t state;
     gz_addr_t rip;
@@ -252,6 +257,7 @@ struct gazelle_stat_lstack_conn_info {
     uint32_t send_ring_cnt;
     uint32_t recv_ring_cnt;
     uint32_t tcp_sub_state;
+
     uint32_t cwn;
     uint32_t rcv_wnd;
     uint32_t snd_wnd;
diff --git a/src/lstack/api/lstack_epoll.c b/src/lstack/api/lstack_epoll.c
index 2f48606..3ad22b9 100644
--- a/src/lstack/api/lstack_epoll.c
+++ b/src/lstack/api/lstack_epoll.c
@@ -650,7 +650,9 @@ int32_t lstack_rtc_epoll_wait(int32_t epfd, struct epoll_event* events, int32_t
             }
         }
         if (tmptimeout > 0) {
-            tmptimeout = update_timeout(tmptimeout, poll_ts);
+            if (tmptimeout <= sys_now() - poll_ts) {
+                tmptimeout = 0;
+            }
         }
 
         loop_flag = false;
diff --git a/src/lstack/api/lstack_wrap.c b/src/lstack/api/lstack_wrap.c
index b19990e..976a3f3 100644
--- a/src/lstack/api/lstack_wrap.c
+++ b/src/lstack/api/lstack_wrap.c
@@ -647,36 +647,31 @@ static int32_t do_select(int32_t nfds, fd_set *readfds, fd_set *writefds, fd_set
     return g_wrap_api->select_fn(nfds, readfds, writefds, exceptfds, timeout);
 }
 
-#define WRAP_VA_PARAM(_fd, _cmd, _lwip_fcntl, _fcntl_fn) \
+#define POSIX_VA_PARAM(fd, cmd, type, lwip_fn, kernel_fn) \
     do { \
-        unsigned long val; \
-        va_list ap; \
-        va_start(ap, _cmd); \
-        val = va_arg(ap, typeof(val)); \
-        va_end(ap); \
-        struct lwip_sock *sock = lwip_get_socket(_fd); \
-        if (select_posix_path() == POSIX_KERNEL || \
-            select_sock_posix_path(sock) == POSIX_KERNEL) \
-            return _fcntl_fn(_fd, _cmd, val); \
-        int32_t ret1 = _fcntl_fn(_fd, _cmd, val); \
-        if (ret1 == -1) { \
-            LSTACK_LOG(ERR, LSTACK, "fd(%d) kernel path call failed, errno is %d, maybe not error\n", \
-                       _fd, errno); \
-            return ret1; \
+        unsigned long __val;    \
+        va_list __ap;           \
+        va_start(__ap, cmd);    \
+        __val = va_arg(__ap, typeof(__val)); \
+        va_end(__ap);           \
+        /* always try kernel */ \
+        int __ret1 = kernel_fn(fd, cmd, __val); \
+        if (__ret1 == -1 || select_sock_posix_path(lwip_get_socket(fd)) == POSIX_KERNEL) { \
+            return __ret1; \
         } \
-        int32_t ret2 = _lwip_fcntl(_fd, _cmd, val); \
+        int __ret2 = lwip_fn(fd, cmd, (type)__val); \
         /* 
          * if function not implemented, fcntl get/set context will not be modifyed by user path,
          * return kernel path result
          */ \
-        if (ret2 == -1) { \
+        if (__ret2 == -1) { \
             if (errno == ENOSYS) { \
-                return ret1; \
+                return __ret1; \
             } \
             LSTACK_LOG(ERR, LSTACK, "fd(%d) user path call failed, errno is %d, maybe not error\n", \
-                       _fd, errno); \
+                       fd, errno); \
         } \
-        return ret2; \
+        return __ret2; \
     } while (0)
 
 /*  --------------------------------------------------------
@@ -701,15 +696,15 @@ int32_t epoll_wait(int32_t epfd, struct epoll_event* events, int32_t maxevents,
 }
 int32_t fcntl64(int32_t s, int32_t cmd, ...)
 {
-    WRAP_VA_PARAM(s, cmd, lwip_fcntl, posix_api->fcntl64_fn);
+    POSIX_VA_PARAM(s, cmd, int, lwip_fcntl, posix_api->fcntl64_fn);
 }
 int32_t fcntl(int32_t s, int32_t cmd, ...)
 {
-    WRAP_VA_PARAM(s, cmd, lwip_fcntl, posix_api->fcntl_fn);
+    POSIX_VA_PARAM(s, cmd, int, lwip_fcntl, posix_api->fcntl_fn);
 }
 int32_t ioctl(int32_t s, int32_t cmd, ...)
 {
-    WRAP_VA_PARAM(s, cmd, lwip_ioctl, posix_api->ioctl_fn);
+    POSIX_VA_PARAM(s, cmd, void*, lwip_ioctl, posix_api->ioctl_fn);
 }
 int32_t accept(int32_t s, struct sockaddr *addr, socklen_t *addrlen)
 {
@@ -853,15 +848,15 @@ int32_t __wrap_epoll_wait(int32_t epfd, struct epoll_event* events, int32_t maxe
 }
 int32_t __wrap_fcntl64(int32_t s, int32_t cmd, ...)
 {
-    WRAP_VA_PARAM(s, cmd, lwip_fcntl, posix_api->fcntl64_fn);
+    POSIX_VA_PARAM(s, cmd, int, lwip_fcntl, posix_api->fcntl64_fn);
 }
 int32_t __wrap_fcntl(int32_t s, int32_t cmd, ...)
 {
-    WRAP_VA_PARAM(s, cmd, lwip_fcntl, posix_api->fcntl_fn);
+    POSIX_VA_PARAM(s, cmd, int, lwip_fcntl, posix_api->fcntl_fn);
 }
 int32_t __wrap_ioctl(int32_t s, int32_t cmd, ...)
 {
-    WRAP_VA_PARAM(s, cmd, lwip_ioctl, posix_api->ioctl_fn);
+    POSIX_VA_PARAM(s, cmd, void*, lwip_ioctl, posix_api->ioctl_fn);
 }
 
 int32_t __wrap_accept(int32_t s, struct sockaddr *addr, socklen_t *addrlen)
diff --git a/src/lstack/core/lstack_cfg.c b/src/lstack/core/lstack_cfg.c
index a1f6c2f..cd81edb 100644
--- a/src/lstack/core/lstack_cfg.c
+++ b/src/lstack/core/lstack_cfg.c
@@ -25,7 +25,6 @@
 
 #include <rte_eth_bond.h>
 #include <lwip/lwipgz_sock.h>
-#include <lwip/lwipgz_posix_api.h>
 #include <lwip/inet.h>
 
 #include "common/gazelle_reg_msg.h"
diff --git a/src/lstack/core/lstack_control_plane.c b/src/lstack/core/lstack_control_plane.c
index f1e3064..11f5129 100644
--- a/src/lstack/core/lstack_control_plane.c
+++ b/src/lstack/core/lstack_control_plane.c
@@ -18,11 +18,14 @@
 #include <sys/socket.h>
 #include <securec.h>
 
-#include <lwip/tcp.h>
 #include <rte_eal.h>
 #include <rte_errno.h>
+#include <rte_cycles.h>
+
+#include <lwip/tcp.h>
 #include <lwip/lwipgz_posix_api.h>
 #include <lwip/lwipgz_flow.h>
+#include <lwip/arch/sys_arch.h>
 
 #include "lstack_cfg.h"
 #include "lstack_dpdk.h"
@@ -353,7 +356,7 @@ static int32_t client_reg_proc_attach(__attribute__((__unused__)) bool is_reconn
     return 0;
 }
 
-static int32_t reg_conn(enum tcp_list_state table_state, enum reg_ring_type reg_type,
+static int32_t reg_conn(enum GAZELLE_TCP_LIST_STATE table_state, enum reg_ring_type reg_type,
     const struct gazelle_stat_lstack_conn *conn)
 {
     struct gazelle_quintuple qtuple;
@@ -370,7 +373,7 @@ static int32_t reg_conn(enum tcp_list_state table_state, enum reg_ring_type reg_
         qtuple.dst_ip = conn->conn_list[i].rip;
         qtuple.dst_port = lwip_htons(conn->conn_list[i].r_port);
 
-        if ((table_state == LISTEN_LIST) && (!match_host_addr((ip_addr_t *)&qtuple.src_ip))) {
+        if ((table_state == GAZELLE_LISTEN_LIST) && (!match_host_addr((ip_addr_t *)&qtuple.src_ip))) {
             continue;
         }
 
@@ -398,16 +401,16 @@ void thread_register_phase1(struct rpc_msg *msg)
     }
 
     struct gazelle_stat_lstack_conn *conn = (struct gazelle_stat_lstack_conn *)msg->args[MSG_ARG_0].p;
-    ret = reg_conn(ACTIVE_LIST, REG_RING_TCP_CONNECT, conn);
+    ret = reg_conn(GAZELLE_ACTIVE_LIST, REG_RING_TCP_CONNECT, conn);
     if (ret != 0) {
-        LSTACK_LOG(ERR, LSTACK, "ACTIVE_LIST rereg conn fail ret=%d\n", ret);
+        LSTACK_LOG(ERR, LSTACK, "GAZELLE_ACTIVE_LIST rereg conn fail ret=%d\n", ret);
         msg->result = ret;
         return;
     }
 
-    ret = reg_conn(TIME_WAIT_LIST, REG_RING_TCP_CONNECT, conn);
+    ret = reg_conn(GAZELLE_TIME_WAIT_LIST, REG_RING_TCP_CONNECT, conn);
     if (ret != 0) {
-        LSTACK_LOG(ERR, LSTACK, "TIME_WAIT_LIST rereg conn fail ret=%d\n", ret);
+        LSTACK_LOG(ERR, LSTACK, "GAZELLE_TIME_WAIT_LIST rereg conn fail ret=%d\n", ret);
     }
     msg->result = ret;
 }
@@ -416,9 +419,9 @@ void thread_register_phase2(struct rpc_msg *msg)
 {
     struct gazelle_stat_lstack_conn *conn = (struct gazelle_stat_lstack_conn *)msg->args[MSG_ARG_0].p;
 
-    int32_t ret = reg_conn(LISTEN_LIST, REG_RING_TCP_LISTEN, conn);
+    int32_t ret = reg_conn(GAZELLE_LISTEN_LIST, REG_RING_TCP_LISTEN, conn);
     if (ret != 0) {
-        LSTACK_LOG(ERR, LSTACK, "LISTEN_LIST rereg conn fail ret=%d\n", ret);
+        LSTACK_LOG(ERR, LSTACK, "GAZELLE_LISTEN_LIST rereg conn fail ret=%d\n", ret);
     }
 
     msg->result = ret;
diff --git a/src/lstack/core/lstack_dpdk.c b/src/lstack/core/lstack_dpdk.c
index a095cbd..e17242c 100644
--- a/src/lstack/core/lstack_dpdk.c
+++ b/src/lstack/core/lstack_dpdk.c
@@ -32,16 +32,14 @@
 #endif
 #include <rte_pdump.h>
 #include <rte_thash.h>
-#include <lwip/lwipgz_posix_api.h>
-#include <lwipopts.h>
-#include <lwip/pbuf.h>
-#include <lwip/lwipgz_flow.h>
-#include <lwip/priv/tcp_priv.h>
-
 #include <rte_eth_bond_8023ad.h>
 #include <rte_eth_bond.h>
 #include <rte_ethdev.h>
 
+#include <lwip/pbuf.h>
+#include <lwip/lwipgz_flow.h>
+#include <lwip/priv/tcp_priv.h>
+
 #include "lstack_log.h"
 #include "common/dpdk_common.h"
 #include "lstack_protocol_stack.h"
diff --git a/src/lstack/core/lstack_init.c b/src/lstack/core/lstack_init.c
index 54ee97e..e89e19c 100644
--- a/src/lstack/core/lstack_init.c
+++ b/src/lstack/core/lstack_init.c
@@ -29,8 +29,6 @@
 #include <lwip/def.h>
 #include <lwip/init.h>
 #include <lwip/lwipgz_sock.h>
-#include <lwip/tcpip.h>
-#include <lwip/lwipgz_memp.h>
 #include <lwip/lwipopts.h>
 #include <lwip/lwipgz_posix_api.h>
 
diff --git a/src/lstack/core/lstack_lwip.c b/src/lstack/core/lstack_lwip.c
index c05a763..271e94f 100644
--- a/src/lstack/core/lstack_lwip.c
+++ b/src/lstack/core/lstack_lwip.c
@@ -12,20 +12,21 @@
 
 #include <sys/types.h>
 #include <stdatomic.h>
+#include <securec.h>
+
+#include <rte_errno.h>
+#include <rte_malloc.h>
+
 #include <lwip/sockets.h>
 #include <lwip/tcp.h>
 #include <lwip/udp.h>
 #include <lwipgz_sock.h>
-#include <arch/sys_arch.h>
 #include <lwip/pbuf.h>
 #include <lwip/priv/tcp_priv.h>
 #include <lwip/lwipgz_posix_api.h>
 #include <lwip/api.h>
 #include <lwip/tcp.h>
 #include <lwip/prot/etharp.h>
-#include <securec.h>
-#include <rte_errno.h>
-#include <rte_malloc.h>
 
 #include "common/gazelle_base_func.h"
 #include "lstack_ethdev.h"
@@ -355,7 +356,7 @@ static inline ssize_t app_buff_write(struct lwip_sock *sock, void *buf, size_t l
     (void)gazelle_ring_read(sock->send_ring, (void **)pbufs, write_num);
 
     if (get_protocol_stack_group()->latency_start) {
-        uint64_t time_stamp = get_current_time();
+        uint64_t time_stamp = sys_now_us();
         time_stamp_into_pbuf(write_num, pbufs, time_stamp);
     }
 
@@ -912,7 +913,7 @@ static bool recv_break_for_err(struct lwip_sock *sock)
 static int recv_ring_get_one(struct lwip_sock *sock, bool noblock, struct pbuf **pbuf)
 {
     int32_t expect = 1; // only get one pbuf
-    uint64_t time_stamp = get_current_time();
+    uint64_t time_stamp = sys_now_us();
 
     if (sock->recv_lastdata != NULL) {
         *pbuf = sock->recv_lastdata;
@@ -1250,20 +1251,20 @@ uint32_t do_lwip_get_conntable(struct gazelle_stat_lstack_conn_info *conn,
     }
 
     for (pcb = tcp_active_pcbs; pcb != NULL && conn_num < max_num; pcb = pcb->next) {
-        conn[conn_num].state = ACTIVE_LIST;
+        conn[conn_num].state = GAZELLE_ACTIVE_LIST;
         copy_pcb_to_conn(conn + conn_num, pcb);
         conn_num++;
     }
 
     for (pcb = tcp_tw_pcbs; pcb != NULL && conn_num < max_num; pcb = pcb->next) {
-        conn[conn_num].state = TIME_WAIT_LIST;
+        conn[conn_num].state = GAZELLE_TIME_WAIT_LIST;
         copy_pcb_to_conn(conn + conn_num, pcb);
         conn_num++;
     }
 
     for (struct tcp_pcb_listen *pcbl = tcp_listen_pcbs.listen_pcbs; pcbl != NULL && conn_num < max_num;
         pcbl = pcbl->next) {
-        conn[conn_num].state = LISTEN_LIST;
+        conn[conn_num].state = GAZELLE_LISTEN_LIST;
         conn[conn_num].lip = *((gz_addr_t *)&pcbl->local_ip);
         conn[conn_num].l_port = pcbl->local_port;
         conn[conn_num].tcp_sub_state = pcbl->state;
diff --git a/src/lstack/core/lstack_protocol_stack.c b/src/lstack/core/lstack_protocol_stack.c
index 9bb1b6a..6f2e84e 100644
--- a/src/lstack/core/lstack_protocol_stack.c
+++ b/src/lstack/core/lstack_protocol_stack.c
@@ -12,15 +12,14 @@
 
 #include <pthread.h>
 #include <stdatomic.h>
+#include <securec.h>
+#include <numa.h>
 
 #include <lwip/sockets.h>
-#include <lwip/tcpip.h>
+#include <lwip/init.h>
 #include <lwip/tcp.h>
-#include <lwip/lwipgz_memp.h>
 #include <lwipgz_sock.h>
 #include <lwip/lwipgz_posix_api.h>
-#include <securec.h>
-#include <numa.h>
 
 #include "common/gazelle_base_func.h"
 #include "lstack_thread_rpc.h"
@@ -332,9 +331,6 @@ static int32_t init_stack_value(struct protocol_stack *stack, void *arg)
     list_init_head(&stack->same_node_recv_list);
     list_init_head(&stack->wakeup_list);
 
-    sys_calibrate_tsc();
-    stack_stat_init();
-
     stack_group->stacks[t_params->idx] = stack;
     set_stack_idx(t_params->idx);
 
@@ -424,7 +420,7 @@ static struct protocol_stack *stack_thread_init(void *arg)
     }
     RTE_PER_LCORE(_lcore_id) = stack->cpu_id;
 
-    tcpip_init(NULL, NULL);
+    lwip_init();
 
     if (use_ltran()) {
         if (client_reg_thrd_ring() != 0) {
diff --git a/src/lstack/core/lstack_stack_stat.c b/src/lstack/core/lstack_stack_stat.c
index 0411d9e..91ca49a 100644
--- a/src/lstack/core/lstack_stack_stat.c
+++ b/src/lstack/core/lstack_stack_stat.c
@@ -31,25 +31,6 @@
 #include "lstack_stack_stat.h"
 #include "lstack_virtio.h"
 
-#define US_PER_SEC  1000000
-
-static uint64_t g_cycles_per_us;
-
-void stack_stat_init(void)
-{
-    uint64_t freq = rte_get_tsc_hz();
-    g_cycles_per_us = (freq + US_PER_SEC - 1) / US_PER_SEC;
-}
-
-uint64_t get_current_time(void)
-{
-    if (g_cycles_per_us == 0) {
-        return 0;
-    }
-
-    return (rte_rdtsc() / g_cycles_per_us);
-}
-
 void time_stamp_transfer_pbuf(struct pbuf *pbuf_old, struct pbuf *pbuf_new)
 {
     if (!get_protocol_stack_group()->latency_start) {
@@ -71,12 +52,12 @@ void time_stamp_transfer_pbuf(struct pbuf *pbuf_old, struct pbuf *pbuf_new)
 
 void time_stamp_into_rpcmsg(struct lwip_sock *sock)
 {
-    sock->stamp.rpc_time_stamp = get_current_time();
+    sock->stamp.rpc_time_stamp = sys_now_us();
 }
 
 void time_stamp_into_recvmbox(struct lwip_sock *sock)
 {
-    sock->stamp.mbox_time_stamp = get_current_time();
+    sock->stamp.mbox_time_stamp = sys_now_us();
 }
 
 void time_stamp_record(int fd, struct pbuf *pbuf)
@@ -108,7 +89,7 @@ void calculate_sock_latency(struct gazelle_stack_latency *stack_latency, struct
         return;
     }
 
-    latency = get_current_time() - stamp;
+    latency = sys_now_us() - stamp;
     latency_stat = &stack_latency->latency[type];
 
     latency_stat->latency_total += latency;
@@ -147,7 +128,7 @@ void calculate_lstack_latency(struct gazelle_stack_latency *stack_latency, const
     }
 
     if (time_record == 0) {
-        lt->stamp_seg[type] = get_current_time() - lt->stamp;
+        lt->stamp_seg[type] = sys_now_us() - lt->stamp;
     } else {
         lt->stamp_seg[type] = time_record > (lt->stamp_seg[type - 1] + lt->stamp) ?
             (time_record - lt->stamp) : lt->stamp_seg[type - 1];
@@ -212,7 +193,7 @@ static void set_latency_start_flag(bool start)
         if (ret != 0) {
             LSTACK_LOG(ERR, LSTACK, "memset_s faile\n");
         }
-        stack->latency.start_time = get_current_time();
+        stack->latency.start_time = sys_now_us();
 
         for (uint32_t j = 0; j < GAZELLE_LATENCY_MAX; j++) {
             stack->latency.latency[j].latency_min = ~((uint64_t)0);
diff --git a/src/lstack/include/lstack_stack_stat.h b/src/lstack/include/lstack_stack_stat.h
index 9fca04a..ef33365 100644
--- a/src/lstack/include/lstack_stack_stat.h
+++ b/src/lstack/include/lstack_stack_stat.h
@@ -31,7 +31,6 @@ void calculate_sock_latency(struct gazelle_stack_latency *stack_latency, struct
 void stack_stat_init(void);
 int handle_stack_cmd(int fd, struct gazelle_stat_msg_request *msg);
 int handle_dpdk_cmd(int fd, enum GAZELLE_STAT_MODE stat_mode);
-uint64_t get_current_time(void);
 void lstack_get_low_power_info(struct gazelle_stat_low_power_info *low_power_info);
 void unregister_wakeup(struct protocol_stack *stack, struct wakeup_poll *wakeup);
 void lstack_calculate_aggregate(int type, uint32_t len);
diff --git a/src/lstack/include/lstack_thread_rpc.h b/src/lstack/include/lstack_thread_rpc.h
index fa98b0c..d268366 100644
--- a/src/lstack/include/lstack_thread_rpc.h
+++ b/src/lstack/include/lstack_thread_rpc.h
@@ -14,7 +14,6 @@
 #define __GAZELLE_THREAD_RPC_H__
 
 #include <pthread.h>
-#include <arch/sys_arch.h>
 #include <rte_mempool.h>
 
 #include "lstack_lockless_queue.h"
diff --git a/src/lstack/netif/lstack_ethdev.c b/src/lstack/netif/lstack_ethdev.c
index d4d0878..cf66e15 100644
--- a/src/lstack/netif/lstack_ethdev.c
+++ b/src/lstack/netif/lstack_ethdev.c
@@ -21,6 +21,7 @@
 #include <lwip/ethip6.h>
 #include <lwip/lwipgz_posix_api.h>
 #include <netif/ethernet.h>
+#include <arch/sys_arch.h>
 
 #include <securec.h>
 
@@ -181,7 +182,7 @@ int32_t eth_dev_poll(void)
     }
 
     if (!use_ltran() && get_protocol_stack_group()->latency_start) {
-        uint64_t time_stamp = get_current_time();
+        uint64_t time_stamp = sys_now_us();
         time_stamp_into_mbuf(nr_pkts, stack->pkts, time_stamp);
     }
 
diff --git a/src/ltran/ltran_forward.c b/src/ltran/ltran_forward.c
index c2f53d6..0de9c1c 100644
--- a/src/ltran/ltran_forward.c
+++ b/src/ltran/ltran_forward.c
@@ -24,7 +24,6 @@
 #include <rte_mempool.h>
 #include <rte_memory.h>
 #include <rte_prefetch.h>
-#include <rte_cycles.h>
 #include <rte_ring.h>
 #include <securec.h>
 
@@ -70,7 +69,7 @@ static void calculate_ltran_latency(struct gazelle_stack *stack, const struct rt
         return;
     }
 
-    latency = get_current_time() - lt->stamp;
+    latency = gazelle_now_us() - lt->stamp;
 
     stack->stack_stats.latency_total += latency;
     stack->stack_stats.latency_pkts++;
@@ -595,7 +594,7 @@ static __rte_always_inline void upstream_forward_loop(uint32_t port_id, uint32_t
     struct rte_mbuf *buf[GAZELLE_PACKET_READ_SIZE] __rte_cache_aligned;
     for (loop_cnt = 0; loop_cnt < UPSTREAM_LOOP_TIMES; loop_cnt++) {
         if (get_start_latency_flag() == GAZELLE_ON) {
-            time_stamp = get_current_time();
+            time_stamp = gazelle_now_us();
         }
 
         rx_count = rte_eth_rx_burst(port_id, queue_id, buf, GAZELLE_PACKET_READ_SIZE);
@@ -657,9 +656,8 @@ void upstream_forward(const uint16_t *port)
     uint32_t queue_num = get_ltran_config()->bond.rx_queue_num;
     uint32_t port_id = get_bond_port()[g_port_index];
     unsigned long now_time;
-    unsigned long last_time = get_current_time();
+    unsigned long last_time = gazelle_now_us();
     unsigned long aging_conn_last_time = last_time;
-    calibrate_time();
 
     while (get_ltran_stop_flag() != GAZELLE_TRUE) {
         for (queue_id = 0; queue_id < queue_num; queue_id++) {
@@ -673,7 +671,7 @@ void upstream_forward(const uint16_t *port)
         }
 #endif
 
-        now_time = get_current_time();
+        now_time = gazelle_now_us();
         if (now_time - aging_conn_last_time > GAZELLE_CONN_INTERVAL) {
             gazelle_delete_aging_conn(gazelle_get_tcp_conn_htable());
             aging_conn_last_time = now_time;
diff --git a/src/ltran/ltran_stat.c b/src/ltran/ltran_stat.c
index 0a8d75c..72de448 100644
--- a/src/ltran/ltran_stat.c
+++ b/src/ltran/ltran_stat.c
@@ -75,7 +75,7 @@ void set_start_latency_flag(int32_t flag)
     }
 
     g_start_latency = flag;
-    g_start_time_stamp = get_current_time();
+    g_start_time_stamp = gazelle_now_us();
 }
 
 int32_t get_start_latency_flag(void)
diff --git a/src/ltran/ltran_stat.h b/src/ltran/ltran_stat.h
index 75cb353..7945d67 100644
--- a/src/ltran/ltran_stat.h
+++ b/src/ltran/ltran_stat.h
@@ -36,12 +36,6 @@ enum GAZELLE_CLIENT_STATE {
     GAZELLE_CLIENT_STATE_MAX
 };
 
-enum GAZELLE_TCP_LIST_STATE {
-    GAZELLE_ACTIVE_LIST,
-    GAZELLE_LISTEN_LIST,
-    GAZELLE_TIME_WAIT_LIST,
-};
-
 enum GAZELLE_TCP_STATE {
     GAZELLE_TCP_STATE_CLS,
     GAZELLE_TCP_STATE_LSN,
diff --git a/src/ltran/ltran_timer.c b/src/ltran/ltran_timer.c
index 51d6544..f1c0e6e 100644
--- a/src/ltran/ltran_timer.c
+++ b/src/ltran/ltran_timer.c
@@ -26,22 +26,16 @@
 #include "ltran_instance.h"
 #include "ltran_timer.h"
 
-static uint64_t g_cycles_per_us = 0;
-
-uint64_t get_current_time(void)
+uint64_t gazelle_now_us(void)
 {
-    if (g_cycles_per_us == 0) {
-        return 0;
+    static uint64_t g_cycles_per_us = 0;
+    if (unlikely(g_cycles_per_us == 0)) {
+        g_cycles_per_us = (rte_get_tsc_hz() + US_PER_S - 1) / US_PER_S;
     }
 
     return (rte_rdtsc() / g_cycles_per_us);
 }
 
-void calibrate_time(void)
-{
-    g_cycles_per_us = (rte_get_tsc_hz() + US_PER_S - 1) / US_PER_S;
-}
-
 void gazelle_detect_sock_logout(struct gazelle_tcp_sock_htable *tcp_sock_htable)
 {
     uint32_t i;
diff --git a/src/ltran/ltran_timer.h b/src/ltran/ltran_timer.h
index abc08b9..6c7aeef 100644
--- a/src/ltran/ltran_timer.h
+++ b/src/ltran/ltran_timer.h
@@ -13,11 +13,11 @@
 #ifndef __GAZELLE_TIMER_H__
 #define __GAZELLE_TIMER_H__
 
+uint64_t gazelle_now_us(void);
+
 struct gazelle_tcp_conn_htable;
 struct gazelle_tcp_sock_htable;
 
-unsigned long get_current_time(void);
-void calibrate_time(void);
 void gazelle_detect_conn_logout(struct gazelle_tcp_conn_htable *conn_htable);
 void gazelle_detect_sock_logout(struct gazelle_tcp_sock_htable *tcp_sock_htable);
 void gazelle_delete_aging_conn(struct gazelle_tcp_conn_htable *conn_htable);
diff --git a/test/unitest/stub.c b/test/unitest/stub.c
index 8f37c90..8478b50 100644
--- a/test/unitest/stub.c
+++ b/test/unitest/stub.c
@@ -10,14 +10,8 @@
  * See the Mulan PSL v2 for more details.
  */
 
-#include <stdint.h>
 #include <stdlib.h>
 
-uint64_t get_current_time(void)
-{
-    return 0;
-}
-
 int rte_pdump_init(void)
 {
     return 0;
-- 
2.33.0

