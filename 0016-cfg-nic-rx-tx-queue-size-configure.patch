From 4adbb49fe002f097683c3c2155387d6fe7d59952 Mon Sep 17 00:00:00 2001
From: jiangheng <jiangheng14@huawei.com>
Date: Mon, 9 Oct 2023 14:21:41 +0800
Subject: [PATCH] cfg: nic rx/tx queue size configure

---
 src/common/gazelle_opt.h                   |  4 ++--
 src/lstack/core/lstack_cfg.c               | 20 ++++++++++++++++++++
 src/lstack/core/lstack_dpdk.c              |  4 ++--
 src/lstack/include/lstack_cfg.h            |  6 ++++++
 src/lstack/include/lstack_protocol_stack.h |  2 +-
 src/lstack/lstack.conf                     |  3 +++
 6 files changed, 34 insertions(+), 5 deletions(-)

diff --git a/src/common/gazelle_opt.h b/src/common/gazelle_opt.h
index 7b6aa19..7b855f9 100644
--- a/src/common/gazelle_opt.h
+++ b/src/common/gazelle_opt.h
@@ -42,8 +42,8 @@
 #define VDEV_TX_QUEUE_SZ            DEFAULT_RING_SIZE
 #define FREE_RX_QUEUE_SZ            DPDK_PKT_BURST_SIZE
 
-#define RTE_TEST_TX_DESC_DEFAULT    2048
-#define RTE_TEST_RX_DESC_DEFAULT    4096
+#define NIC_QUEUE_SIZE_MAX          8192
+#define NIC_QUEUE_SIZE_MIN          512
 
 #define TCP_CONN_COUNT              1500
 #define MBUF_COUNT_PER_CONN         170
diff --git a/src/lstack/core/lstack_cfg.c b/src/lstack/core/lstack_cfg.c
index 9fc16dd..0eca86e 100644
--- a/src/lstack/core/lstack_cfg.c
+++ b/src/lstack/core/lstack_cfg.c
@@ -74,6 +74,8 @@ static int32_t parse_use_bond4(void);
 static int32_t parse_bond4_slave_mac(void);
 static int32_t parse_use_sockmap(void);
 static int32_t parse_udp_enable(void);
+static int32_t parse_nic_rxqueue_size(void);
+static int32_t parse_nic_txqueue_size(void);
 
 #define PARSE_ARG(_arg, _arg_string, _default_val, _min_val, _max_val, _ret) \
     do { \
@@ -131,6 +133,8 @@ static struct config_vector_t g_config_tbl[] = {
     { "bond4_slave_mac", parse_bond4_slave_mac },
     { "use_sockmap", parse_use_sockmap },
     { "udp_enable", parse_udp_enable },
+    { "nic_rxqueue_size", parse_nic_rxqueue_size},
+    { "nic_txqueue_size", parse_nic_txqueue_size},
     { NULL,           NULL }
 };
 
@@ -1163,3 +1167,19 @@ static int32_t parse_use_sockmap(void)
     PARSE_ARG(g_config_params.use_sockmap, "use_sockmap", 0, 0, 1, ret);
     return ret;
 }
+
+static int32_t parse_nic_rxqueue_size(void)
+{
+    int32_t ret;
+    PARSE_ARG(g_config_params.nic.rxqueue_size, "nic_rxqueue_size", 4096,
+              NIC_QUEUE_SIZE_MIN, NIC_QUEUE_SIZE_MAX, ret);
+    return ret;
+}
+
+static int32_t parse_nic_txqueue_size(void)
+{
+    int32_t ret;
+    PARSE_ARG(g_config_params.nic.txqueue_size, "nic_txqueue_size", 2048,
+              NIC_QUEUE_SIZE_MIN, NIC_QUEUE_SIZE_MAX, ret);
+    return ret;
+}
diff --git a/src/lstack/core/lstack_dpdk.c b/src/lstack/core/lstack_dpdk.c
index 902dadc..a53e85c 100644
--- a/src/lstack/core/lstack_dpdk.c
+++ b/src/lstack/core/lstack_dpdk.c
@@ -370,8 +370,8 @@ static struct eth_params *alloc_eth_params(uint16_t port_id, uint16_t nb_queues)
 
     eth_params->port_id = port_id;
     eth_params->nb_queues = nb_queues;
-    eth_params->nb_rx_desc = RTE_TEST_RX_DESC_DEFAULT;
-    eth_params->nb_tx_desc = RTE_TEST_TX_DESC_DEFAULT;
+    eth_params->nb_rx_desc = get_global_cfg_params()->nic.rxqueue_size;
+    eth_params->nb_tx_desc = get_global_cfg_params()->nic.txqueue_size;
     eth_params->conf.link_speeds = ETH_LINK_SPEED_AUTONEG;
     eth_params->conf.txmode.mq_mode = ETH_MQ_TX_NONE;
     eth_params->conf.rxmode.mq_mode = ETH_MQ_RX_NONE;
diff --git a/src/lstack/include/lstack_cfg.h b/src/lstack/include/lstack_cfg.h
index 52a1abd..e48a501 100644
--- a/src/lstack/include/lstack_cfg.h
+++ b/src/lstack/include/lstack_cfg.h
@@ -59,6 +59,11 @@ struct secondary_attach_arg {
     char file_prefix[PATH_MAX];
 };
 
+struct cfg_nic_params {
+    uint32_t rxqueue_size;
+    uint32_t txqueue_size;
+};
+
 struct cfg_params {
     ip4_addr_t host_addr;
     ip4_addr_t netmask;
@@ -110,6 +115,7 @@ struct cfg_params {
     uint8_t bond4_slave2_mac_addr[ETHER_ADDR_LEN];
     bool use_sockmap;
     bool udp_enable;
+    struct cfg_nic_params nic;
 };
 
 struct cfg_params *get_global_cfg_params(void);
diff --git a/src/lstack/include/lstack_protocol_stack.h b/src/lstack/include/lstack_protocol_stack.h
index b7c18d3..a4f6ac2 100644
--- a/src/lstack/include/lstack_protocol_stack.h
+++ b/src/lstack/include/lstack_protocol_stack.h
@@ -69,7 +69,7 @@ struct protocol_stack {
     uint32_t rx_ring_used;
     uint32_t tx_ring_used;
 
-    struct rte_mbuf *pkts[RTE_TEST_RX_DESC_DEFAULT];
+    struct rte_mbuf *pkts[NIC_QUEUE_SIZE_MAX];
     struct list_node recv_list;
     struct list_node same_node_recv_list; /* used for same node processes communication */
     struct list_node wakeup_list;
diff --git a/src/lstack/lstack.conf b/src/lstack/lstack.conf
index fa7e284..ad574e1 100644
--- a/src/lstack/lstack.conf
+++ b/src/lstack/lstack.conf
@@ -34,6 +34,9 @@ rpc_number = 4
 #read nic pkts number
 nic_read_number = 128
 
+nic_rxqueue_size = 4096
+nic_txqueue_size = 2048
+
 #each cpu core start a protocol stack thread.
 num_cpus="2"
 
-- 
2.27.0

