%define conf_path %{_sysconfdir}/%{name}

Name:          gazelle
Version:       1.0.2
Release:       84
Summary:       gazelle is a high performance user-mode stack
License:       MulanPSL-2.0
URL:           https://gitee.com/openeuler/gazelle
Source0:       %{name}-%{version}.tar.gz

BuildRequires: cmake gcc-c++
BuildRequires: lwip >= 2.2.0-63
BuildRequires: dpdk-devel >= 21.11-5
BuildRequires: numactl-devel libpcap-devel libconfig-devel libboundscheck uthash-devel
%ifarch ppc64le
BuildRequires: libarchive libarchive-devel
%endif

Requires:      dpdk >= 21.11-5
Requires:      numactl libpcap libconfig libboundscheck iproute

Patch9001:     0001-remove-unused-dpdk-dynamic-libraries.patch
Patch9002:     0002-gazelle_stat_pkts-gazelle_stat_low_power_info.patch
Patch9003:     0003-set-localhost-ip-when-bind-ip-is-same-as-ip-in-lstac.patch
Patch9004:     0004-refactoring-preload-module.patch
Patch9005:     0005-fix-previous-commit-refactoring-preload-module.patch
Patch9006:     0006-add-lstack_preload.c-to-makefile.patch
Patch9007:     0007-cosmetic-changes.patch
Patch9008:     0008-add-loongarch64-and-sw64-arch-support.patch
Patch9009:     0009-CFG-fixed-the-dpdk-primary-default-parameter.patch
Patch9010:     0010-suport-clang-build.patch
Patch9011:     0011-ethdev-fix-pbuf-chain-tot_len-incorrect.patch
Patch9012:     0012-kernelevent-kernel-event-thread-report-kernel-events.patch
Patch9013:     0013-fix-bond-port-reta_size-init.patch
Patch9014:     0014-init-remove-sync-sem-between-lstack-thread-and-main-.patch
Patch9015:     0015-lstack_lwip-external-api-start-with-do_lwip_-prefix.patch
Patch9016:     0016-cfg-nic-rx-tx-queue-size-configure.patch
Patch9017:     0017-epoll-distinguish-add-del_sock_event-and-add-del_soc.patch
Patch9018:     0018-preload-support-thread-hijacking-mode.patch
Patch9019:     0019-cfg-add-run-to-completion-mode-configure.patch
Patch9020:     0020-statck-the-for-loop-in-lstack-thread-is-defined-as-s.patch
Patch9021:     0021-delete_rule-bug.patch
Patch9022:     0022-tools-gazelle_setup-adapt-non-ltran-mode.patch
Patch9023:     0023-wrap-add-run-to-completion-wakeup-mode-api.patch
Patch9024:     0024-fix-arping-gazelle-return-value-is-1.patch
Patch9025:     0025-init-stack-setup-in-app-thread-when-app-call-socket-.patch
Patch9026:     0026-epoll-adapt-epoll-interface-for-rtc-mode.patch
Patch9027:     0027-clean-useless-code.patch
Patch9028:     0028-ethdev-fix-arp-unicast-packets-cannot-be-transmitted.patch
Patch9029:     0029-stack-add-semaphore-to-ensure-all-stack-threads-setu.patch
Patch9030:     0030-ethdev-register-offload-to-netif.patch
Patch9031:     0031-epoll-fix-epollet-mode-error.patch
Patch9032:     0032-bond6.patch
Patch9033:     0033-cfg-fix-bond_mode-null.patch
Patch9034:     0034-dfx-add-four-snmp-udp-datas.patch
Patch9035:     0035-cfg-nic-queue-size-only-support-power-of-2.patch
Patch9036:     0036-stack-fix-possible-coredump-when-arp-packet-broadcas.patch
Patch9037:     0037-Fix-coredump-issue-and-skip-rte_pdump_init-for-secon.patch
Patch9038:     0038-solve-problem-that-rte_pktmbuf_poll_creat-in-same-nu.patch
Patch9040:     0040-when-timeout-occurs-process-exits.patch
Patch9041:     0041-wrap-support-select.patch
Patch9042:     0042-add-vlan-support.patch
Patch9043:     0043-slave-mac-addr.patch
Patch9044:     0044-PRE_LOG-modify-prelog-info-transfer-to-terminal-at-t.patch
Patch9045:     0045-ethdev-mbuf-data-start-pointer-pbuf-payload.patch
Patch9047:     0047-cfg-fix-lstack-mempool-lookup-failed-in-ltran-mode.patch
Patch9048:     0048-add-tx-package-timeout.patch
Patch9049:     0049-modif-mem.patch
Patch9050:     0050-enable-ipv6.patch
Patch9051:     0051-wrap-dont-hijack-select-temporarily.patch
Patch9052:     0052-ipv6.patch
Patch9053:     0053-add-gazelle-log.patch
Patch9054:     0054-PRE_LOG-modify-log-info-while-there-are-repeated-ite.patch
Patch9055:     0055-add-gazelle-log.patch
Patch9056:     0056-wrap-fix-connect-wrong-port-after-addr-bind-two-port.patch
Patch9057:     0057-cfg-rm-ipv6_enable.patch
Patch9058:     0058-ltran-support-vlan.patch
Patch9059:     0059-update-src-ltran-ltran_param.c.patch
Patch9060:     0060-dfx-support-bond-get-dpdk-xstats.patch
Patch9061:     0061-add-tcp-faste-timer-cnt.patch
Patch9062:     0062-stack-fix-coredump-caused-by-gazelleclt-in-rtc-mode.patch
Patch9063:     0063-dfx-add-tcp-exception-and-kernel-event-statistics.patch
Patch9064:     0064-add-vlan_id-in-netif.patch
Patch9065:     0065-support-vlan-offload.patch
Patch9066:     0066-gazellectl-fix-gazellectl-lstack-show-1-r-error.patch
Patch9067:     0067-fix-coredump-because-sock-closed-before-send-data-fu.patch
Patch9068:     0068-fix-compilation-error.patch
Patch9069:     0069-fix-coredump-because-of-addr-null-in-lwip_fill_sendr.patch
Patch9070:     0070-add-struct-gz_addr_t.patch
Patch9071:     0071-wrap-support-shutdown.patch
Patch9072:     0072-1.solve-the-problem-of-1w-connection-not-being-able-.patch
Patch9073:     0073-dfx-gazellectl-c-support-ipv6.patch
Patch9074:     0074-The-call-stack-is-not-printed-in-the-proactive-exit-.patch
Patch9075:     0075-dfx-fix-gazellectl-lstack-show-ip-failed.patch
Patch9076:     0076-gazellectl-add-connect-para.patch
Patch9077:     0077-log-optimize-lstack-log.patch
Patch9078:     0078-support-show-nic-offload-and-features.patch
Patch9079:     0079-Fixed-an-issue-where-no-packet-is-sent-or-received-w.patch
Patch9080:     0080-fix-example-print-error.patch
Patch9081:     0081-dfx-fix-kernel_events-stat.patch
Patch9082:     0082-add-keep-alive-info.patch
Patch9083:     0083-fix-close-can-t-exit.patch
Patch9084:     0084-mod-unix-time-stamp-to-local-time.patch
Patch9085:     0085-optimize-gazelle-exit-process.patch
Patch9086:     0086-fix-EPOLLIN-event-error.patch
Patch9087:     0087-mod-time-err.patch
Patch9088:     0088-fix-gazellectl-lstack-show-ip-r-with-ltran-error-log.patch
Patch9089:     0089-fix-udp-multicast-bind-error.patch
Patch9090:     0090-support-netperf.patch
Patch9091:     0091-fix-alloc-not-free.patch
Patch9092:     0092-sigaction-fix-deprecated-signal-flags.patch
Patch9093:     0093-fix-gazellectl-c-msg-error.patch
Patch9094:     0094-CFG-fix-check-func-strdup-return-value-is-NULL.patch
Patch9095:     0095-fix-func-separate_str_to_array-overflow-problem.patch
Patch9096:     0096-use-default-nonblock-mode.patch
Patch9097:     0097-fix-rte_ring_create-free-time-consuming.patch
Patch9098:     0098-optimize-shutdown-exit-process.patch
Patch9099:     0099-fix-func-separate_str_to_array-overflow-problem.patch
Patch9100:     0100-fix-func-separate_str_to_array-overflow-problem.patch
Patch9101:     0101-fix-func-separate_str_to_array-overflow-problem.patch
Patch9102:     0102-fix-func-separate_str_to_array-overflow-problem.patch
Patch9103:     0103-fix-func-stack_setup_thread-array-doesn-t-init.patch
Patch9104:     0104-fix-stack_setup_thread-array-range.patch
Patch9105:     0105-fix-func-separate_str_to_array-overflow-problem.patch
Patch9106:     0106-fix-dpdk_alloc_pktmbuf-time-consuming.patch
Patch9107:     0107-ltran-optimize-config-file-error-message.patch
Patch9108:     0108-replace-with-gz_addr_t.patch
Patch9109:     0109-match_host_addr-func-support-ipv6.patch
Patch9110:     0110-add-example-keep-alive-interval-para.patch
Patch9111:     0111-update-src-common-dpdk_common.c.patch
Patch9112:     0112-listen_shadow-support-ipv6.patch
Patch9113:     0113-lstack_dpdk-limit-mbuf-max-num.patch
Patch9114:     0114-gazellectl-add-tcp_input-empty-ack-cnt.patch
Patch9115:     0115-add-socket-accept-fail-cnt.patch
Patch9116:     0116-diff-lstack-and-ltran-dfx-sock.patch
Patch9117:     0117-fix-host_addr6-can-be-assigned-a-multicast-address.patch
Patch9118:     0118-udp-do-not-merge-data-into-last-pbuf.patch
Patch9119:     0119-adpat-dpdk-23.11.patch
Patch9120:     0120-modify-conf-vlan-default-vlaue.patch
Patch9121:     0121-remove-unused-variables-in-pbuf-and-reduce-mbuf-size.patch
Patch9122:     0122-optimize-recv-exit-process-for-FIN-and-unsupport-SO_.patch
Patch9123:     0123-remove-expand_send_ring.patch
Patch9124:     0124-set-ltran.patch
Patch9125:     0125-add-bond-doc.patch
Patch9126:     0126-cfg-host_addr-is-not-mandatory.patch
Patch9127:     0127-add-bond1-support.patch
Patch9128:     0128-fix-t_params-double-free.patch
Patch9129:     0129-fix-receive-fin-packet-process-error.patch
Patch9130:     0130-support-netperf-UDP_STREAM-and-UDP_RR.patch
Patch9131:     0131-adapt-lwip-2.2.0.patch
Patch9132:     0132-add-observable-method-of-data-aggregation.patch
Patch9133:     0133-try-to-ensure-arp-packet-can-be-sent.patch
Patch9134:     0134-gazellectl-support-send-latency-show.patch
Patch9135:     0135-rpc-function-does-not-depend-on-protocol-stack.patch
Patch9136:     0136-readv-return-1-errno-is-EAGAIN-when-recvring-no-data.patch
Patch9137:     0137-split-the-flow-fules-related-functions-into-separate.patch
Patch9138:     0138-fix-coreddump-when-stack-setup-failed-in-rtc-mode.patch
Patch9139:     0139-diff-udp-and-tcp-read-from-stack.patch
Patch9140:     0140-FAULT-INJECT-gazelle-add-packet-delay-and-packet-dro.patch
Patch9141:     0141-recv-support-MSG_DONTWAIT.patch
Patch9142:     0142-do_setsockopt-function-no-longer-checks-the-results-.patch
Patch9143:     0143-FAUT-INJECT-add-fault-inject-unset-method.patch
Patch9144:     0144-support-udp-pkglen-mtu.patch
Patch9145:     0145-add-limit-with-level-for-sockopt.patch
Patch9146:     0146-udp-add-restriction-message-too-long.patch
Patch9147:     0147-dfx-improve-log-readability-when-connect-ltran-lstac.patch
Patch9148:     0148-fix-rpc_pool-create-failed-coredump.patch
Patch9149:     0149-ensure-the-bond-interface-flow_type_rss_offloads-mat.patch
Patch9150:     0150-FAULT-INJECT-add-duplicate-and-reorder-methods.patch
Patch9151:     0151-select-timeout-arguments-check.patch
Patch9152:     0152-recvfrom-support-timeout.patch
Patch9153:     0153-fix-netperf-setsockopt-fail.patch
Patch9154:     0154-fix-LATENCY_WRITE-increase-in-recv-process.patch
Patch9155:     0155-dpdk-add-vlan-filter.patch
Patch9156:     0156-Correcting-spelling-errors.patch
Patch9157:     0157-perftool-add-lhist-statstic-tool.patch
Patch9158:     0158-add-udp-poll.patch
Patch9159:     0159-DFX-adapt-testcase-for-gazellectl-connect-failed.patch
Patch9160:     0160-warp-add-configuration-check-with-host_addr-and-serv.patch
Patch9161:     0161-add-latency-nodes-READ_APP_CALL-WRITE_RPC_MSG.patch
Patch9162:     0162-fix-vlan-filter-can-be-added-when-vlan_mode-1.patch
Patch9163:     0163-Add-support-for-arch-ppc64le.patch
Patch9164:     0164-dfx-support-get-nic-bond-status.patch
Patch9165:     0165-remove-dpdk_skip_nic_init-for-dpdk-23.11.patch
Patch9166:     0166-gazellectl-add-lwip-stats_proto-print.patch
Patch9167:     0167-fix-port-not-support-vlan-filter.patch
Patch9168:     0168-fix-tcp-recv-does-not-return-pkg-when-ring-buffer-is.patch
Patch9169:     0169-add-tuple_fileter-error-info.patch
Patch9170:     0170-adapt-dpdk-23.11-remove-kni.patch
Patch9171:     0171-fix-ioctl-set-failed.patch
Patch9172:     0172-ltran-memset-quintuple.patch
Patch9173:     0173-gazellectl-add-lwip-stats_proto-print.patch
Patch9174:     0174-CFG-set-multicast-IP-assert.patch
Patch9175:     0175-cfg-devices-must-be-in-bond_slave_mac-for-BONDING_MO.patch
Patch9176:     0176-CFG-fix-multicast-IP-assert-error.patch
Patch9177:     0177-fix-mbuf-leak-in-dpdk-23.11-due-to-kni-removed.patch
Patch9178:     0178-add-riscv64-support.patch
Patch9179:     0179-dfx-fix-gazellectl-x-for-bond.patch
Patch9180:     0180-change-gazelle_stat_lstack_proto-from-u16-to-u64.patch
Patch9181:     0181-memary-error-fix-some-memary-error.patch
Patch9182:     0182-bond-remove-bond-initialization-code-in-dpdk_ethdev_.patch
Patch9183:     0183-make-rpc_msg_max-recv_ring_size-configurable.patch
Patch9184:     0184-EPOLL-fix-coredump-while-event-count-exceed-maxevent.patch
Patch9185:     0185-fix-fin-pack-free-coredump.patch
Patch9186:     0186-fix-MySQL-shutdown-cmd.patch
Patch9187:     0187-cfg-remove-map-perfect-flag-in-lstack.conf.patch
Patch9188:     0188-redis-perf-add-tx-driver-cache.patch
Patch9189:     0189-optimized-latency-distribution-dotting.patch
Patch9190:     0190-rtc-adapt-rtc_close.patch
Patch9191:     0191-virtio-flow_bifurcation-switch.patch
Patch9192:     0192-remove-legacy-mem.patch
Patch9193:     0193-cfg-bond_slave_mac-support-pci-addr.patch
Patch9194:     0194-refactor-tx-cache-module.patch
Patch9195:     0195-virtio-create-and-init-virtio_port.patch
Patch9196:     0196-refactor-udp-send.patch
Patch9197:     0197-solve-compile-err-in-20.03.patch
Patch9198:     0198-fix-function-call-error.patch
Patch9199:     0199-perftool-add-latency-tool.patch
Patch9200:     0200-cfg-del-unnecessary-logs.patch
Patch9201:     0201-fix-dpdk_bond_primary_set-bug.patch
Patch9202:     0202-fix-build-failed-in-2003sp4.patch
Patch9203:     0203-virtio-cfg-ipv4-and-ipv6-addr.patch
Patch9204:     0204-parse-packages-type-in-rx_poll.patch
Patch9205:     0205-virtio-distribute-pkg-by-dst_port.patch
Patch9206:     0206-fix-coredump-when-get-empty-from-udp-sendring.patch
Patch9207:     0207-fix-poll-init-not-clear-old-fd.patch
Patch9208:     0208-virtio-mode-actual_queue_num.patch
Patch9209:     0209-virtio-update-g_rule_port-by-reg_ring_type-enum.patch
Patch9210:     0210-virtio-dfx-data-of-virtio.patch
Patch9211:     0211-add-flow_bifurcation-switch-in-lstack_cfg-file.patch
Patch9212:     0212-example-sync-example-update.patch
Patch9213:     0213-cleancode-improving-makefile-readability.patch
Patch9214:     0214-add-.gitignore.patch
Patch9215:     0215-cleancode-rename-gazelle-files-in-lwip.patch
Patch9216:     0216-cleancode-refactor-lwipsock.h.patch
Patch9217:     0217-cleancode-refactor-posix-type-and-get_socket.patch
Patch9218:     0218-fix-some-error-of-NULL-pointer.patch
Patch9219:     0219-cleancode-refactor-posix_api.patch
Patch9220:     0220-cleancode-refactor-lwipgz_list.h.patch
Patch9221:     0221-fix-EPOLLIN-event-dropd.patch
Patch9222:     0222-cleancode-refactor-lwipgz_hlist.h.patch
Patch9223:     0223-add-sem-post-when-update-event.patch
Patch9224:     0224-cleancode-refactor-sys_now-and-lwip_ioctl.patch
Patch9225:     0225-POSIX-fix-select-timeout-disable-and-build-failed-in.patch
Patch9226:     0226-support-kernel-connect.patch
Patch9227:     0227-Check-the-return-of-lwip_init.patch
Patch9228:     0228-vitio_user-modify-mbuf-index-for-bond4.patch
Patch9229:     0229-fix-redis-coredump-when-hugetlbs-pagesize-is-1024M.patch
Patch9230:     0230-dfx-optimize-gazellectl-x-for-bond.patch
Patch9231:     0231-virtio-fix-dfx-error-with-multiple-cpus.patch
Patch9232:     0232-fix-issue-create-virtio_user-based-on-bond4-main-net.patch
Patch9233:     0233-virtio_user-modify-mbuf-index-for-bond4.patch
Patch9234:     0234-WRAP-fix-double-connect-lead-posix-api-disable.patch
Patch9235:     0235-virtio_user-add-vlan-info-for-kernerl-packets-when-v.patch
Patch9236:     0236-virtio_user-solve-the-issue-that-failed-to-bind-virt.patch
Patch9237:     0237-refector-fill-udp-sendring.patch
Patch9238:     0238-virtio_user-The-program-establishes-a-network-connec.patch
Patch9239:     0239-WRAP-fix-ltran-mode-did-not-bind-kernel-while-open-k.patch
Patch9240:     0240-WRAP-fix-bind-log-error.patch
Patch9241:     0241-virtio-mod-virtio_user_name-when-multi-process-is-on.patch
Patch9242:     0242-fix-redis-coredump-ctrl-c-during-the-pressure-test.patch 
Patch9243:     0243-virtio_user-check-netif-status.patch
Patch9244:     0244-virtio-solve-compilation-error-in-2003sp3.patch
Patch9245:     0245-CMAKE-fix-ltran-build-error-in-2403.patch
Patch9246:     0246-fix-mbuf_total-calculation-error.patch
Patch9247:     0247-cfg-modify-maximum-tcp_conn_count-to-2w.patch
Patch9248:     0248-tools-fix-sync-patch-script-date-wrong-and-update-th.patch
Patch9249:     0249-epoll-fix-wild-pointer-detected-by-cooddy.patch
Patch9250:     0250-tools-fix-script-generate-patchname-wrong.patch
Patch9251:     0251-fix-when-errno-is-ENOTCONN-ignore-it.patch
Patch9252:     0252-rtc-do_lwip_init_sock-no-need-to-create-ring-in-rtc-.patch
Patch9253:     0253-example-solve-double-free.patch
Patch9254:     0254-WRAP-support-setsockopt-SO_SNDTIMEO-SO_SNBUF.patch
Patch9255:     0255-DFX-adapt-log-optimization.patch
Patch9256:     0256-LOG-add-log-when-udp-send_ring-is-exhausted.patch
Patch9257:     0257-cleancode-refactor-rtc_api-rtw_api-and-dummy_api.patch
Patch9258:     0258-cleancode-move-some-API-from-stack-to-rpc-and-rtw.patch
Patch9259:     0259-cleancode-add-rpc_async_call-remove-rpc_msg_arg.sock.patch
Patch9260:     0260-cleancode-declare-different-cfg_params-types.patch
Patch9261:     0261-Fill-in-a-portion-of-mbuf-to-send_ring-when-mbuf-is-.patch
Patch9262:     0262-add-pingpong-dfx-support.patch
Patch9263:     0263-add-interrupt-mode-support.patch
Patch9264:     0264-af_xdp-set-rlimit-unlimit-when-gazelle-init.patch
Patch9265:     0265-fix-stack-null-when-register-interrupt.patch
Patch9266:     0266-rtw-fix-send-length-exceeding-send_ring_size.patch
Patch9267:     0267-rpc-fix-rpc_sync_call-spinlock-block-when-msg-be-rec.patch
Patch9268:     0268-bugfix-fix-gazelle-init-failed-while-setup-by-non-ro.patch
Patch9269:     0269-xdp-skip-checksum-temporarily-due-to-kernel-cannot-t.patch
Patch9270:     0270-fix-dpdk_nic_is_xdp-coredump-in-ltran-mode.patch
Patch9271:     0271-fix-the-coredump-when-gazellectl-l.patch
Patch9272:     0272-control-call-epoll_ctl-delete-fd-when-fd-close.patch
Patch9273:     0273-epoll-remove-unnecessary-judgment-code.patch
Patch9274:     0274-slove-compile-err-when-GAZELLE_TCP_REUSE_IPPORT-is-o.patch
Patch9275:     0275-bugfix-start-fail-when-executing-the-popen-command-f.patch
Patch9276:     0276-Fix-annotation-errors.patch
Patch9277:     0277-remove-the-unused-return-variable.patch
Patch9278:     0278-add-SO_NUMA_ID-optname-for-adapting-opneGauss.patch
Patch9279:     0279-xdp-support-XDP_STATISTICS-by-posix_api-getsockopt_f.patch
Patch9280:     0280-interrupt-fix-timeout-events-cannot-be-counted.patch
Patch9281:     0281-remove-code-about-nobolck-mode-for-mysql.patch
Patch9282:     0282-LOG-Optimize-some-log-displays.patch
Patch9283:     0283-xdp-support-bind-no-cpu-mode.patch
Patch9284:     0284-support-auto-set-xdp-addr.patch
Patch9285:     0285-suport-kernel-accept-for-openGauss.patch
Patch9286:     0286-openGauss-support-kernel-accept4.patch
Patch9287:     0287-socket-init-wakeup-in-blocking-socket.patch
Patch9288:     0288-fix-socket-of-control-thread-is-overwirtten-due-to-a.patch
Patch9289:     0289-LWIP-adjust-position-of-shutdown-in-callback-of-conn.patch
Patch9290:     0290-Fix-the-wrong-spelling-description-in-the-notes-and-.patch
Patch9291:     0291-update-test-unitest-ltran-ltran_param_test.c.patch
Patch9292:     0292-fix-free-null-pointer-when-no-matching-device-is-fou.patch
Patch9293:     0293-cfg-show-dpdk-args-after-dpdk_adjust_args.patch
Patch9294:     0294-fix-build-error-in-2003SP4.patch
Patch9295:     0295-CFG-fix-xdp-iface-check-error.patch
Patch9296:     0296-xdp-support-stack-bind-numa.patch
Patch9297:     0297-openGauss-support-kernel-connnect.patch
Patch9298:     0298-DUMP-gazelle-supports-dump-lstack.patch
Patch9299:     0299-openGauss-fix-gs_ctl-switchover-failed.patch
Patch9300:     0300-openGauss-fix-connection-attempt-failed.patch
Patch9301:     0301-remove-app_bind_numa-check-from-exclude_cpus.patch
Patch9302:     0302-fix-rpc-pool-leak-when-thread-exits.patch
Patch9303:     0303-fix-epoll-and-recv-threads-blocked-on-the-same-semap.patch
Patch9304:     0304-fix-errno-ETIMEFOUT.patch
Patch9305:     0305-cfg-notify-that-it-s-unsupported-when-stack_num-1.patch
Patch9306:     0306-fix-a-contention-issue-when-rpc-pools-are-added-to-r.patch
Patch9307:     0307-openGauss-unsupport_tcp_optname.patch
Patch9308:     0308-kernerl-bind-add-ipv6-add-check.patch
Patch9309:     0309-Connect-execute-lwip-connect-if-dst_ip-and-host_ip-a.patch
Patch9310:     0310-DUMP-fix-build-error-of-oe2003-because-of-micro-is-n.patch
Patch9311:     0311-SIGNAL-Adjust-sigaction-function-to-keep-lstack-sign.patch
Patch9312:     0312-SIGNAL-Adjust-hijack-sigal-table-to-hijack-SIGABRT-S.patch
Patch9313:     0313-DUMP-fix-abnomal-printing-in-the-dump-process.patch
Patch9314:     0314-fix-the-memory-leak-when-using-strdup.patch
Patch9315:     0315-Stack-unset-stack_stop-while-stacks-exit-by-rpc-mess.patch
Patch9316:     0316-SIGNAL-block-SIGSEGV-during-exit-process.patch
Patch9317:     0317-add-xdp-tx-checksum-tso-offload.patch
Patch9318:     0318-RTC-mode-fix-gazellectl-can-t-print-connenct-info.patch
Patch9319:     0319-Connect-fix-benchmark_dws-connect-failed.patch
Patch9320:     0320-Protocal-fixing-deathlock-between-protocol-threads-a.patch
Patch9321:     0321-update-gazelle-max-numa-nodes-8.patch

%description
%{name} is a high performance user-mode stack.

ExclusiveArch: x86_64 aarch64 ppc64le riscv64

%prep
%autosetup -n %{name}-%{version} -p1

%build
#export DPDK_VERSION_1911=1
cd %{_builddir}/%{name}-%{version}
# Add compile option, ignore map address check. Scenarios: asan test
%if 0%{?gazelle_map_addr_nocheck}
    sed -i 's/-pthread/-pthread -D gazelle_map_addr_nocheck/' %{_builddir}/%{name}-%{version}/src/ltran/CMakeLists.txt
%endif
sh build/build.sh

%install
install -dpm 0755 %{buildroot}/%{_bindir}
install -dpm 0755 %{buildroot}/%{_prefix}/lib64
install -dpm 0750 %{buildroot}/%{conf_path}

install -Dpm 0500 %{_builddir}/%{name}-%{version}/src/lstack/liblstack.*     %{buildroot}/%{_libdir}/
install -Dpm 0640 %{_builddir}/%{name}-%{version}/src/lstack/lstack.Makefile %{buildroot}/%{conf_path}/
install -Dpm 0640 %{_builddir}/%{name}-%{version}/src/lstack/lstack.conf     %{buildroot}/%{conf_path}/

install -Dpm 0500 %{_builddir}/%{name}-%{version}/src/ltran/gazellectl       %{buildroot}/%{_bindir}/
install -Dpm 0500 %{_builddir}/%{name}-%{version}/src/ltran/ltran            %{buildroot}/%{_bindir}/
install -Dpm 0640 %{_builddir}/%{name}-%{version}/src/ltran/ltran.conf       %{buildroot}/%{conf_path}/

%files
%defattr(-,root,root)
%dir %{conf_path}
%{_bindir}/*
%{_libdir}/liblstack.*
%{conf_path}/lstack.Makefile
%config(noreplace) %{conf_path}/lstack.conf
%config(noreplace) %{conf_path}/ltran.conf

%changelog
* Tue Mar 04 2025 yinbin6 <yinbin8@huawei.com> - 1.0.2-84
- update gazelle max numa nodes 8
- Protocal: fixing deathlock between protocol threads and app thread
- Connect: fix benchmark_dws connect failed
- RTC-mode: fix gazellectl can't print connenct info
- add xdp tx checksum/tso offload

* Fri Jan 17 2025 yinbin6 <yinbin8@huawei.com> - 1.0.2-83
- SIGNAL: block SIGSEGV during exit process
- Stack: unset stack_stop, while stacks exit by rpc message.
- fix the memory leak when using strdup

* Fri Jan 10 2025 yinbin6 <yinbin8@huawei.com> - 1.0.2-82
- DUMP: fix abnomal printing in the dump process.
- SIGNAL: Adjust hijack sigal table to hijack SIGABRT SIGQUIT and delet SIGKILL
- SIGNAL: Adjust sigaction function to keep lstack signal function executed successfully.

* Thu Dec 19 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-81
- DUMP: fix build error of oe2003 because of micro is not defined.

* Wed Dec 18 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-80
- Connect: execute lwip connect if dst_ip and host_ip are in the same network.
- kernerl bind: add ipv6 add check
- openGauss unsupport_tcp_optname
- fix a contention issue when rpc pools are added to rpc_pool_array

* Wed Dec 11 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-79
- cfg: notify that it's unsupported, when stack_num > 1
- fix errno == ETIMEFOUT
- fix epoll and recv threads blocked on the same semaphore. data cannot be read in recv thread.
- fix rpc pool leak, when thread exits
- remove app_bind_numa check from exclude_cpus
- openGauss: fix connection attempt failed
- openGauss: fix gs_ctl switchover failed

* Wed Dec 04 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-78
- DUMP: gazelle supports dump lstack

* Wed Nov 27 2024 yinbin <yinbin8@huawei.com> - 1.0.2-77
- openGauss: support kernel connnect
- xdp: support stack bind numa
- CFG: fix xdp iface check error
- fix build error in 2003SP4
- cfg: show dpdk args after dpdk_adjust_args
- fix free null pointer when no matching device is found
- update test/unitest/ltran/ltran_param_test.c.
- Fix the wrong spelling description in the notes and logs
- LWIP: adjust position of shutdown in callback of connect
- fix socket of control thread is overwirtten due to another primary process start

* Wed Nov 20 2024 yinbin <yinbin8@huawei.com> - 1.0.2-76
- socket: init wakeup in blocking socket
- openGauss: support kernel accept4

* Fri Nov 15 2024 yinbin <chengyechun1@huawei.com> - 1.0.2-75
- suport kernel accept for openGauss
- support auto set xdp addr
- xdp: support bind no cpu mode

* Fri Nov 08 2024 yinbin <jiangheng14@huawei.com> - 1.0.2-74
- LOG: Optimize some log displays
- remove: code about nobolck mode for mysql
- interrupt: fix timeout events cannot be counted

* Fri Nov 01 2024 hantwofish <yinbin8@huawei.com> - 1.0.2-73
- xdp: support XDP_STATISTICS by posix_api->getsockopt_fn
- add SO_NUMA_ID(optname) for adapting opneGauss
- remove the unused return variable.
- Fix annotation errors
- bugfix: start fail when executing the popen command for the second time in openGauss

* Sat Oct 26 2024 hantwofish <hankangkang5@huawei.com> - 1.0.2-72
- slove compile err when GAZELLE_TCP_REUSE_IPPORT is off

* Wed Oct 23 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-71
- epoll: remove unnecessary judgment code
- control: call epoll_ctl delete fd when fd close

* Fri Oct 11 2024 yangchen <yangchen145@huawei.com> - 1.0.2-70
- fix the coredump when gazellectl -l

* Thu Oct 10 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-69
- fix dpdk_nic_is_xdp coredump in ltran mode

* Thu Oct 10 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-68
- xdp: skip checksum temporarily due to kernel cannot transfer offloads

* Wed Oct 09 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-67
- bugfix: fix gazelle init failed while setup by non-root user
- rpc: fix rpc_sync_call spinlock block when msg be recalled
- rtw: fix send length exceeding send_ring_size
- fix stack null when register interrupt

* Sun Sep 29 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-66
- af_xdp: set rlimit unlimit when gazelle init
- add interrupt mode support
- add pingpong dfx support

* Fri Sep 27 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-65
- Fill in a portion of mbuf to send_ring, when mbuf is insufficient.

* Fri Sep 27 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-64
- cleancode: declare different cfg_params types
- cleancode: add rpc_async_call, remove rpc_msg_arg.socklen, fix some format
- cleancode: move some API from stack to rpc and rtw
- cleancode: refactor rtc_api rtw_api and dummy_api

* Fri Sep 27 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-63
- LOG:add log when udp send_ring is exhausted
- DFX: adapt log optimization

* Fri Sep 20 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-62
- WRAP: support setsockopt SO_SNDTIMEO SO_SNBUF

* Fri Sep 13 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-61
- example: solve double free
- rtc: do_lwip_init_sock no need to create ring in rtc mode

* Fri Sep 06 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-60
- fix: when errno is ENOTCONN, ignore it
- tools: fix script generate patchname wrong

* Fri Aug 30 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-59
- epoll: fix wild pointer detected by cooddy
- tools: fix sync patch script date wrong and update the way get patchnum
- cfg: modify maximum tcp_conn_count to 2w
- fix mbuf_total calculation error

* Wed Aug 28 2024 laokz <zhangkai@iscas.ac.cn> - 1.0.2-58
- add riscv64 to %ExclusiveArch

* Mon Aug 26 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-57
- CMAKE: fix ltran build error in 2403

* Fri Aug 23 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-56
- virtio: solve compilation error in 2003sp3
- virtio_user: check netif status

* Fri Aug 16 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-55
- virtio: mod virtio_user_name when multi process is on
- WRAP: fix bind log error
- WRAP:fix ltran mode did not bind kernel while open kni
- virtio_user: The program establishes a network connection when network card status is up
- fix redis coredump ctrl-c during the pressure test 

* Fri Aug 9 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-54
- refector fill udp sendring
- virtio_user: solve the issue that failed to bind virtio_user's IPv6 address
- virtio_user: add vlan info for kernerl packets when vlan switch is on
- WRAP:fix double connect lead posix api disable

* Fri Aug 2 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-53
- virtio_user: modify mbuf index for bond4
- fix issue: create virtio_user based on bond4 main network card
- virtio: fix dfx error with multiple cpus
- dfx: optimize gazellectl -x for bond

* Thu Jul 25 2024 yangchen555 <yangchen145@huawei.com> - 1.0.2-52
- fix redis coredump when hugetlbs pagesize is 1024M
- vitio_user: modify mbuf index for bond4
- Check the return of lwip_init
- support kernel connect
- POSIX: fix select timeout disable and build failed in openEuler 2003

* Fri Jul 19 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-51
- cleancode: refactor sys_now and lwip_ioctl
- add sem post when update event
- cleancode: refactor lwipgz_hlist.h
- fix EPOLLIN event dropd
- cleancode: refactor lwipgz_list.h
- cleancode: refactor posix_api
- fix some error of NULL pointer
- cleancode: refactor posix type and get_socket
- cleancode: refactor lwipsock.h

* Thu Jul 11 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-50
- cleancode: rename gazelle files in lwip
- add .gitignore
- cleancode: improving makefile readability

* Thu Jul 11 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-49
- example: sync example update

* Fri Jul 5 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-48
- add flow_bifurcation switch in lstack_cfg file
- virtio: dfx data of virtio
- virtio: update g_rule_port by reg_ring_type enum
- virtio: mode actual_queue_num
- fix poll init not clear old fd

* Fri Jun 28 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-47
- fix coredump when get empty from udp sendring
- [virtio]: distribute pkg by dst_port
- parse packages type in rx_poll
- [virtio]: cfg ipv4 and ipv6 addr
- fix build failed in 2003sp4
- fix dpdk_bond_primary_set bug
- cfg: del unnecessary logs
- perftool: add latency tool

* Wed Jun 26 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-46
- fix function call error

* Tue Jun 25 2024 yinbin<yinbin6@huawei.com> - 1.0.2-45
- fix changelog version incorrect

* Tue Jun 25 2024 yinbin<yinbin6@huawei.com> - 1.0.2-44
- sync solve compile err in 20.03

* Fri Jun 14 2024 jiangheng <jiangheng14@huawei.com> - 1.0.2-43
- refacotr udp send

* Fri Jun 21 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-42
- [virtio]: create and init virtio_port
- refactor tx cache module
- cfg: bond_slave_mac support pci addr
- remove legacy-mem
- [virtio]: flow_bifurcation switch
- rtc: adapt rtc_close
- optimized latency distribution dotting
- redis perf: add tx driver cache

* Fri Jun 14 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-41
- cfg: remove map-perfect flag in lstack.conf
- fix MySQL shutdown cmd
- fix fin pack free coredump

* Fri June 7 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-40
- make rpc_msg_max recv_ring_size configurable
- EPOLL: fix coredump while eventcount exceed maxevent

* Fri May 31 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-39
- bond:remove bond initialization code in dpdk_ethdev_init

* Fri May 24 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-38
- memary error: fix some memary error
- change gazelle_stat_lstack_proto from u16 to u64

* Thu May 16 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-37
- dfx: fix gazellectl -x for bond

* Fri May 10 2024 yangchen555 <yangchen145@huawei.com> - 1.0.2-36
- add riscv64 support
- fix mbuf leak in dpdk-23.11 due to kni removed

* Thu May 9 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-35
- CFG:fix multicast IP assert error
- cfg: devices must be in bond_slave_mac for BONDING_MODE_ACTIVE
- CFG:set multicast IP assert
- gazellectl add lwip stats_proto print
- ltran: memset quintuple

* Sun Apr 28 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-34
- fix ioctl set failed

* Fri Apr 19 2024 yangchen555 <yangchen145@huawei.com> - 1.0.2-33
- adapt dpdk-23.11 remove kni
- add tuple_fileter error info
- fix tcp recv does not return pkg when ring buffer is empty

* Fri Apr 12 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-32
- fix port not support vlan filter
- gazellectl add lwip stats_proto print
- remove dpdk_skip_nic_init for dpdk-23.11
- dfx: support get nic bond status

* Sun Apr 7 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-31
- Add support for arch ppc64le

* Sun Apr 7 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-30
- fix vlan filter can be added when vlan_mode=-1
- add latency nodes: READ_APP_CALL & WRITE_RPC_MSG
- warp: add configuration check with host_addr and server_ip for ipv6
- DFX: adapt testcase for gazellectl connect failed
- add udp poll
- perftool: add lhist statstic tool
- Correcting spelling errors
- dpdk add vlan filter
- fix LATENCY_WRITE increase in recv process
- fix netperf setsockopt fail
- recvfrom support timeout
- select timeout arguments check
- FAULT INJECT: add duplicate and reorder methods
- ensure the bond interface flow_type_rss_offloads match slave

* Thu Mar 14 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-29
- fix rpc_pool create failed coredump
- dfx: improve log readability when connect ltran/lstack failed
- udp add restriction: message too long
- add limit with level for sockopt
- support udp pkglen > mtu
- FAUT INJECT: add fault inject unset method
- do_setsockopt function no longer checks the results of the kernel.
- recv support MSG_DONTWAIT

* Thu Mar 14 2024 yinbin6 <jiangheng14@huawei.com> - 1.0.2-28
- FAULT INJECT: gazelle add packet delay and packet drop
- diff udp and tcp read from stack
- fix coreddump when stack setup failed in rtc mode
- split the flow fules related functions into separate file
- readv return -1, errno is EAGAIN when recvring no data
- rpc function does not depend on protocol stack diff rpc queue and dfx rpc queue
- gazellectl: support send latency show
- try to ensure arp packet can be sent
- add observable method of data aggregation

* Wed Feb 07 2024 yibin6 <yinbin8@huawei.com> - 1.0.2-27
- support netperf UDP_STREAM and UDP_RR

* Sun Feb 4 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-26
- support netperf UDP_STREAM and UDP_RR
- fix receive fin packet process error
- fix t_params double free
- add bond1 support
- cfg: host_addr is not mandatory
- add bond doc
- set ltran
- remove expand_send_ring
- optimize recv exit process for FIN and unsupport SO_RCVBUF
- remove unused variables in pbuf and reduce mbuf size
- modify conf vlan default vlaue

* Sat Jan 20 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-25
- adpat dpdk 23.11
- udp: do not merge data into last pbuf
- fix host_addr6 can be assigned a multicast address
- diff lstack and ltran dfx sock
- add socket accept fail cnt
- gazellectl: add tcp_input empty ack cnt
- lstack_dpdk: limit mbuf max num
- listen_shadow support ipv6

* Sat Jan 06 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-24
- replace with gz_addr_t
- update src/common/dpdk_common.c
- match_host_addr func support ipv6
- add example keep-alive interval para

* Sat Jan 06 2024 yinbin6 <yinbin8@huawei.com> - 1.0.2-23
- fix dpdk_alloc_pktmbuf time-consuming
- ltran: optimize config file error message

* Wed Dec 27 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-22
- fix rte_ring_create/free time consuming
- use default nonblock mode
- fix func separate_str_to_array overflow problem
- CFG:fix check func strdup return value is NULL
- fix gazellectl -c msg error
- sigaction: fix deprecated signal flags
- fix alloc not free
- optimize shutdown exit process

* Tue Dec 19 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-21
- support netperf
- fix udp multicast bind error

* Mon Dec 18 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-20
- fix gazellectl lstack show ip -r with ltran error && log info display unknow error
- mod time err

* Sat Dec 16 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-19
- fix EPOLLIN event error
- optimize gazelle exit process 1. close all fds 2. lstack thread exits, then gazelle process exits
- mod unix time stamp to local time
- fix close can't exit
- add keep-alive info
- dfx: fix kernel_events stat

* Sat Dec 9 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-18
- Fixed an issue where no packet is sent or received when UDP traffic is sent
- support show nic offload and features
- log: optimize lstack log
- gazellectl add connect para
- dfx: fix 'gazellectl lstack show ip' failed
- The call stack is not printed in the proactive exit scenario.
- dfx: gazellectl -c support ipv6
- 1.solve the problem of 1w connection not being able to ping 2.add debug info : rpc_mempool availd size
- wrap: support shutdown
- add struct gz_addr_t
- fix coredump because of addr null in lwip_fill_sendring
- fix compilation error
- fix coredump because sock closed before send data fully
- gazellectl: fix gazellectl lstack show 1 -r error

* Wed Nov 29 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-17
- support vlan offload
- add vlan_id in netif
- dfx: add tcp exception and kernel event statistics
- stack: fix coredump caused by gazelleclt in rtc mode when stack num defined is greater than the hijacked thread num
- add tcp faste timer cnt
- dfx: support bond get dpdk xstats
- update src/ltran/ltran_param.c. 规范679行的报错日志信息，将“,”之后的“Please”修正为“please”
- ltran support vlan
- cfg: rm ipv6_enable
- wrap: fix connect wrong port after addr bind two ports
- add gazelle log
- PRE_LOG: modify log info while there are repeated items in conf file
- add gazelle log

* Wed Nov 22 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-16
- ipv6

* Sat Nov 18 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-15
- wrap: dont hijack select temporarily

* Sat Nov 18 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-14
- enable ipv6

* Sat Nov 18 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-13
- modif mem
- add tx package timeout
- cfg: fix lstack mempool lookup failed in ltran mode
- build: fix ltran build error
- ethdev: mbuf data start pointer pbuf->payload
- PRE_LOG: modify prelog info transfer to terminal at the same time
- slave mac addr
- add vlan support
- wrap: support select
- when timeout occurs,process exits.
- build: add mlx5 pmd dependency

* Sat Nov 18 2023 hantwofish <hankangkang5@huawei.com> - 1.0.2-12
- solve problem that rte_pktmbuf_poll_creat in same numa .
- Fix coredump issue and skip rte_pdump_init for secondary process
- stack: fix possible coredump when arp packet broadcast in rtc mode
- cfg: nic queue size only support power of 2
- dfx: add four snmp udp datas

* Sat Nov 4 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-11
- cfg: fix bond_mode null

* Sat Nov 4 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-10
- bond6
- epoll: fix epollet mode error
- ethdev: register offload to netif
- stack: add semaphore to ensure all stack threads setup success in rtw mode before call main()
- ethdev: fix arp unicast packets cannot be transmitted to current procotol stack
- clean useless code
- epoll: adapt epoll interface for rtc mode

* Sat Nov 4 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-9
- init: stack setup in app thread when app call socket/epoll_create first in rtc mode
- fix arping gazelle return value is 1
- wrap: add run-to-completion/wakeup mode api
- tools: gazelle_setup adapt non ltran mode
- delete_rule bug
- statck: the for loop in lstack thread is defined as stack_polling

* Mon Oct 30 2023 yangchenCloud <yangchen145@huawei.com> - 1.0.2-8
- cfg: add run-to-completion mode configure
- preload: support thread hijacking mode

* Mon Oct 30 2023 hantwofish <hankangkang5@huawei.com> - 1.0.2-7
- epoll: distinguish add/del_sock_event and add/del_sock_event_nolock
- cfg: nic rx/tx queue size configure

* Sat Oct 14 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-6
- update lwip version buildrequire

* Fri Oct 13 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-5
- lstack_lwip: external api start with do_lwip_ prefix
- init: remove sync sem between lstack thread and main thread
- fix bond port reta_size init

* Sat Oct 7 2023 yinbin6 <yinbin8@huawei.com> - 1.0.2-4
- kernelevent: kernel event thread report kernel events to app thread directly without passing through lstack thread
- ethdev: fix pbuf chain tot_len incorrect p->tot_len = p->len + (p->next ? p->next->tot_len : 0)
- suport clang build
- CFG:fixed the dpdk primary default parameter
- 添加龙芯和申威架构支持
- cosmetic changes

* Sat Aug 19 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.2-3
- add lstack_preload.c to makefile fix compile failure
- fix previous commit "refactoring preload module"
- refactoring preload module
- set localhost ip when bind ip is same as ip in lstack.conf

* Sat Jul 8 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.2-2
- sync remove unused dpdk dynamic libraries

* Tue Jul 4 2023 jiangheng12 <chengyechun1@huawei.com> - 1.0.2-1
- update version to 1.0.2

* Mon Jul 3 2023 jiangheng12 <chengyechun1@huawei.com> - 1.0.1-72
- bond4 add dpdk return value check

* Sat Jul 1 2023 jiangheng12 <chengyechun1@huawei.com> - 1.0.1-71
- fix bond4 EPOLLOUT event error

* Fri Jun 30 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-70
- add build requires constraints on lwip version

* Tue Jun 27 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-69
- add udp_enable to turn off udp in need

* Wed Jun 21 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-68
- check return value of hugepage_init to avoid coredump when hugepage memory is insufficient
- fix core dump when slave mac failed

* Sun Jun 25 2023 kircher <majun65@huawei.com> - 1.0.1-67
- enable UDP CKSUM in gazelle
- lstack: cfg add app_exclude_cpus
- skip gro when tcp/ip checksum offloads disable
- fix null pointer of sock in udp_recvfrom

* Mon Jun 19 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-66
- add multicast enable in dpdk_ethdev_init
- add use_sockmap in cfg to distinguish whether to use dpdk ring communication between processes

* Mon Jun 19 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-65
- remove obsolete args num_wakeup in doc

* Thu Jun 15 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-64
- change send_ring_size 32 in lstack conf
- adapt to dpdk-19.11 and dpdk-21.11
- fix t_params use after free in kernel event thread
- rpc pool use dpdk mempool replace array

* Wed Jun 14 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-63
- set sock when select path is PATH_UNKNOW

* Wed Jun 14 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-62
- fix udp send/recv in muliple queue
- fix gazellectl block before lstack registration is complete
- add exception handling for is_dst_ip_localhost
- fix change low power mod invalid
- send/recv thread bind numa only app_bind_numa set to 1

* Tue Jun 6 2023 jiangheng <jiangheng14@huawei.com> - 1.0.1-61
- revert cleancode series patches

* Fri Jun 2 2023 LemmyHuang <huangliming5@huawei.com> - 1.0.1-60
- fix socket init and clean
- fix coredump caused by lwip_get_socket_nouse

* Mon May 29 2023 LemmyHuang <huangliming5@huawei.com> - 1.0.1-59
- drop netbuf in read_lwip_data to fix mem overflow
- cleancode: refactor memp
- cleancode: refactor sys_now and lwip_ioctl
- cleancode: refactor gazelle_hlist.h
- cleancode: refactor gazelle_list.h
- cleancode: refactor gazelle_posix_api.h
- cleancode: refactor lwipsock.h
- cleancode: rename gazelle files in lwip
- cleancode: improving makefile readability

* Sat May 20 2023 jiangheng <jiangheng14@huawei.com> - 1.0.1-58
- modify hugepage directory name

* Tue May 16 2023 kircher <majun65@huawei.com> - 1.0.1-57
- add udp multicast support in gazelle
- clean code
- add bond4 suport
- dfx: security function failed, return error directly
- dfx: set g_unix_fd to -1 after abnormal close fd to avoid double close
- fix bond_ports parse error fix socket_mem parse error when the value exceeds 65536 exit if create_rxtx_pktmbuf failed build.sh build failed return 1 clean code
- add parentheses to fix build error
- fix rpc msg alloc failed fix process_numa args error coredump fix sock->conn not free when fd is kernel mode
- kni down not stop nic
- fix client connect number unbalance on lstack
- fix gazellectl -x error when multiplt user nic config

* Sat May 13 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-56
- fix build err with dpdk-21.11
- fix config flow rule race
- update lstack.Makefile
- add socket check before write it
- optimize do_close
- set kni_switch valid only in primary process
- fix build err on select_path
- optimite select_path and pbuf_take
- check primary process idx and secondary lstack num
- revert select_thread_path and optimize app thread when sendmsg
- do not transfer broadcast arp pkts to other process

* Thu Apr 20 2023 sunsuwan <sunsuwan3@huawei.com> - 1.0.1-55
- sepeate_string_to array add error args handle

* Thu Apr 20 2023 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-54
- waiting when primary process not start already
- gazelle send/recv thread bind numa

* Tue Mar 21 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-53
- fix parse args error
- discard wakeup_num parameter
- fix kernel scoket select path error

* Sat Mar 18 2023 jiangheng<jiangheng14@huawei.com> - 1.0.1-52
- add pbuf lock when aggregate pbuf
- support multi process
- add gazellectl -x -a args
- add same node ring for iner-process communication
- fix send reset by peer when not sleep after connect
- add tuple filter in conf to diff rss rule adn tuple filter rule
- support tuple rule add/delete
- disable tso without ipv4 checksum
- refactor mbuf private data

* Sat Mar 11 2023 kircher <majun65@huawei.com> - 1.0.1-51
- when send ring full whether dynamic alloc mbuf is configurable reduce cpu usage when send ring full
- send should return -1, errno EAGAIN when ring full
- send ring size is configure
- remove rxtx driver cache
- fix do_close core dump
- fix private data offset error

* Mon Feb 27 2023 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-50
- reduce duplicate code in lstack_cfg.c
- adapt unsupport sock optname

* Wed Feb 22 2023 kircher <majun65@huawei.com> - 1.0.1-49
- eneble TSO and fix TSO mbuf pktlen error
- check and fix wakeup_list when null appears

* Mon Feb 13 2023 net <jiangheng14@huawei.com> - 1.0.1-48
- change mbuf_pool_size in lstack.conf to tcp_conn_count * mbuf_count_per_conn
- bring up kni when init
- fix coredump in example server mum mode
- add fucntest
- test readv writev epoll_create1 accept4

* Mon Feb 6 2023 jiangheng12 <jiangheng14@huawei.com> - 1.0.1-47
- add gazelle setup tools
- add unitest

* Tue Jan 31 2023 kircher <majun65@huawei.com> - 1.0.1-46
- add gazelle fuzz
- add log message when wait for connecting to ltran

* Mon Jan 16 2023 kircher <majun65@huawei.com> - 1.0.1-45
- add ret check in pthread_create and fix example bug
- move control_client_thread creation after control_in and dpdk_skip_nic_init

* Fri Jan 6 2023 kircher <majun65@huawei.com> - 1.0.1-44
- add the suggestion of using the -u parameter when the connection to unix socket fails

* Fri Dec 30 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-43
- revert expand recv data buff

* Wed Dec 28 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-42
- pbuf cacheline align
  support main thread affinity
  reduce epoll wakeup

* Fri Dec 23 2022 kircher <majun65@huawei.com> - 1.0.1-41
- fix null pointer deref in stack_broadcast_close
- fix lstack Makefile warning

* Thu Dec 22 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-40
- add dfx rcv_nxt info
  mbuf private cache line align
  send pkts index bug fix
  free recv pkts in main loop

* Wed Dec 21 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-39
- add mempool dfx info
  write support without epoll/poll

* Tue Dec 20 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-38
- optimite recv data buff and send pkts index

* Sun Dec 18 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-37
- pkts-bulk-send-to-nic and rpc-dont-send

* Sat Dec 17 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-36
- remove mbuf reserve in mbuf alloc

* Sat Dec 17 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-35
- optimite net type
  app bind numa when epoll/poll create
  stack thread params set dafult value

* Sat Dec 17 2022 kircher <majun65@huawei.com> - 1.0.1-34
- add RXTX_NB_MBUF_MAX to limit mbuf_pool_size to its range

* Fri Dec 16 2022 kircher <majun65@huawei.com> - 1.0.1-33
- move select_thread_path after posix_api_init

* Thu Dec 15 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-32
- ltran rxtx mbuf pool size config by conf

* Thu Dec 15 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-31
- stack thread params config by lstack.conf

* Wed Dec 14 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-30
- fix kernel event thread bind nuam failed

* Tue Dec 13 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-29
- dfx add pcb windows info
  rxtx mbuf pool size config by conf

* Mon Dec 12 2022 kircher <majun65@huawei.com> - 1.0.1-28
- add pdump support in ltran

* Sat Dec 3 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-27
- optimize app thread write buff block

* Fri Dec 2 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-26
- fix epoll_wait report events 0

* Thu Dec 1 UTC compile_success <980965867@qq.com> - 1.0.1-25
- add malloc init zero
- modify EPOLLOUT event is overwritten.

* Sat Nov 26 UTC compile_success <980965867@qq.com> - 1.0.1-24
- fix write event error

* Wed Nov 16 2022 kircher <majun65@huawei.com> - 1.0.1-23
- modify duplicate code
- fix data flow error when use NIC in kernel
- fix lwip send return 0 add err event
- fix pcb snd_buf flip
- avoid send stop when mbuf pool empty
- merge lstack rx tx mbuf pool

* Tue Nov 15 2022 kircher <majun65@huawei.com> - 1.0.1-22
- fix pdump and mutil NIC init fail

* Mon Nov 14 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-21
- support muti-nic
  fix some bugs

* Tue Nov 8 2022 kircher <majun65@huawei.com> - 1.0.1-20
- add pdump support in lstack

* Sat Nov 07 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-19
- resolve the conflict between the eth_dev_ops variable and the dpdk-19.11

* Sat Nov 05 2022 kircher <majun65@huawei.com> - 1.0.1-18
- Add kni local support in lstack

* Fri Nov 04 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-17
- Optimize ceph client performance

* Sat Oct 08 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-16
- refactor event
  addapt for ceph client

* Mon Sep 05 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-15
- expand rpc msg pool size

* Mon Sep 05 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-14
- backport bugfix and doc

* Mon Aug 08 2022 fushanqing <fushanqing@kylinos.cn> - 1.0.1-13
- Unified license name specification

* Tue Jul 26 2022 wuchangsheng <wuchangsheng2@huawei.com> - 1.0.1-12
- support epoll oneshot 

* Tue Jul 19 2022 xiusailong <xiusailong@huawei.com> - 1.0.1-11
- reconstruct packet sending and receiving to improve performance 

* Thu Jul 7 2022 jiangheng <jiangheng14@huawei.com> - 1.0.1-10
- Type:bugfix
- CVE:
- SUG:NA
- DESC:update readme
       fix some bugs
       refactor pkt read send to improve performance
       refactoe kernle event to improve performanc 

* Fri May 27 2022 xiusailong <xiusailong@huawei.com> - 1.0.1-9
- update license lockless queue

* Fri May 20 2022 xiusailong <xiusailong@huawei.com> - 1.0.1-8
- update README.md

* Thu Mar 31 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-7
- Type:bugfix
- CVE:
- SUG:NA
- DESC:add gazelle.yaml

* Tue Mar 29 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-6
- refactor event
- add gazellectl lstack constraint

* Fri Mar 18 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-5
- limit lwip_alloc_pbuf size to TCP_MSS
- sending of sock last data is triggered by lstack iteself 

* Thu Mar 17 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-4
- fix repeatede stack restart coredump

* Wed Mar 16 2022 jiangheng <jiangheng12@huawei.com> - 1.0.1-3
- fix gazelle test issue

* Mon Mar 7 2022 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-2
- reduce copy in send

* Thu Mar 3 2022 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.1-1
- support mysql with two mode:ltran+lstack and lstack.

* Thu Feb 24 2022 wu-changsheng <wuchangsheng2@huawei.com> - 1.0.0-1
- release initial version
